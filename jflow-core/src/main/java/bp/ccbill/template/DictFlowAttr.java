package bp.ccbill.template;

import bp.en.EntityMyPKAttr;

public class DictFlowAttr extends EntityMyPKAttr {
    /// <summary>
    /// 表单ID
    /// </summary>
    public static final String FrmID = "FrmID";
    /// <summary>
    /// 子流程编号
    /// </summary>
    public static final String FlowNo = "FlowNo";
    /// <summary>
    /// 标签
    /// </summary>
    public static final String Label = "Label";
    /// <summary>
    /// 是否显示在表格右边
    /// </summary>
    public static final String IsShowListRight = "IsShowListRight";
    /// <summary>
    /// Idx
    /// </summary>
    public static final String Idx = "Idx";
}
