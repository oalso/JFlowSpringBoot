package bp.ccbill.template;

import bp.en.EntitiesNoName;
import bp.en.Entity;

import java.util.ArrayList;
import java.util.List;

public class GroupMethods extends EntitiesNoName {
    ///构造
    /**
     单据属性s
     */
    public GroupMethods()
    {
    }
    /**
     得到它的 Entity
     */
    @Override
    public Entity getGetNewEntity()
    {
        return new GroupMethod();
    }

    ///


    ///为了适应自动翻译成java的需要,把实体转换成List.
    /**
     转化成 java list,C#不能调用.

     @return List
     */
    public final List<GroupMethod> ToJavaList()
    {
        return (List<GroupMethod>)(Object)this;
    }
    /**
     转化成list

     @return List
     */
    public final ArrayList<GroupMethod> Tolist()
    {
        ArrayList<GroupMethod> list = new ArrayList<GroupMethod>();
        for (int i = 0; i < this.size(); i++)
        {
            list.add((GroupMethod)this.get(i));
        }
        return list;
    }

    /// 为了适应自动翻译成java的需要,把实体转换成List.
}
