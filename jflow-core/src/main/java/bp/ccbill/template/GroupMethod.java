package bp.ccbill.template;

import bp.en.EntityNoName;
import bp.en.Map;
import bp.en.UAC;
import bp.web.WebUser;

public class GroupMethod extends EntityNoName {
    ///权限控制.
    @Override
    public UAC getHisUAC() throws Exception
    {
        UAC uac = new UAC();
        if (WebUser.getNo().equals("admin") == true)
        {
            uac.IsDelete = false;
            uac.IsUpdate = true;
            return uac;
        }
        uac.Readonly();
        return uac;
    }
    /*
    表单ID
     */
    public final String getFrmID() throws Exception
    {
        return this.GetValStrByKey(GroupMethodAttr.FrmID);
    }
    public final void setFrmID(String value) throws Exception
    {
        this.SetValByKey(GroupMethodAttr.FrmID, value);
    }
    /*
   表单类型
    */
    public final String getMethodType() throws Exception
    {
        return this.GetValStrByKey(GroupMethodAttr.MethodType);
    }
    public final void setMethodType(String value) throws Exception
    {
        this.SetValByKey(GroupMethodAttr.MethodType, value);
    }
    /*
   顺序号
    */
    public final String getIdx() throws Exception
    {
        return this.GetValStrByKey(GroupMethodAttr.Idx);
    }
    public final void setIdx(String value) throws Exception
    {
        this.SetValByKey(GroupMethodAttr.Idx, value);
    }
    /*
   图标
    */
    public final String getIcon() throws Exception
    {
        return this.GetValStrByKey(GroupMethodAttr.Icon);
    }
    public final void setIcon(String value) throws Exception
    {
        this.SetValByKey(GroupMethodAttr.Icon, value);
    }
    /*
   图标
    */
    public final String getMethodID() throws Exception
    {
        return this.GetValStrByKey(GroupMethodAttr.MethodID);
    }
    public final void setMethodID(String value) throws Exception
    {
        this.SetValByKey(GroupMethodAttr.MethodID, value);
    }
    public GroupMethod()
    {
    }
    /**
     EnMap
     */
    @Override
    public Map getEnMap() throws Exception {
        if (this.get_enMap() != null) {
            return this.get_enMap();
        }
        Map map = new Map("Frm_GroupMethod", "方法分组");
        map.AddTBStringPK(GroupMethodAttr.No, null, "编号", true, true, 0, 500, 20);
        map.AddTBString(GroupMethodAttr.FrmID, null, "表单ID", true, true, 0, 200, 20);

        map.AddTBString(GroupMethodAttr.Name, null, "标签", true, false, 0, 500, 20, true);
        map.AddTBString(GroupMethodAttr.Icon, null, "Icon", true, true, 0, 200, 20, true);

        map.AddTBInt(GroupMethodAttr.Idx, 0, "顺序号", true, false);
        map.AddTBAtParas(3000);
        this.set_enMap(map);
        return this.get_enMap();
    }
}
