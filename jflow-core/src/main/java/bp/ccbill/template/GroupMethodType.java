package bp.ccbill.template;

public class GroupMethodType {
    /// <summary>
    /// 自定义
    /// </summary>
    public static final String Self = "Self";
    /// <summary>
    /// 基础资料变更
    /// </summary>
    public static final String FlowBaseData = "FlowBaseData";
    /// <summary>
    /// 新创建实体
    /// </summary>
    public static final String FlowNewEntity = "FlowNewEntity";
    /// <summary>
    /// 其他业务组件
    /// </summary>
    public static final String FlowFlowEtc = "FlowFlowEtc";
}
