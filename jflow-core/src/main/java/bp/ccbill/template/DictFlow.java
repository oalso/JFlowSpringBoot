package bp.ccbill.template;

import bp.en.EntityMyPK;
import bp.en.Map;

public class DictFlow extends EntityMyPK {
    /*
    表单ID
     */
    public final String getFrmID() throws Exception
    {
        return this.GetValStringByKey(DictFlowAttr.FrmID);
    }
    public final void setFrmID(String value) throws Exception
    {
        SetValByKey(DictFlowAttr.FrmID, value);
    }
    /*
    流程编号
     */
    public final String getFlowNo() throws Exception
    {
        return this.GetValStringByKey(DictFlowAttr.FlowNo);
    }
    public final void setFlowNo(String value) throws Exception
    {
        SetValByKey(DictFlowAttr.FlowNo, value);
    }
    /*
   显示标签
    */
    public final String getLabel() throws Exception
    {
        return this.GetValStringByKey(DictFlowAttr.Label);
    }
    public final void setLabel(String value) throws Exception
    {
        SetValByKey(DictFlowAttr.Label, value);
    }
    /*
   是否显示在表格右边
    */
    public final int getIsShowListRight() throws Exception
    {
        return this.GetValIntByKey(DictFlowAttr.IsShowListRight);
    }
    public final void setIsShowListRight(int value) throws Exception
    {
        SetValByKey(DictFlowAttr.IsShowListRight, value);
    }
    /*
  顺序号
   */
    public final int getIdx() throws Exception
    {
        return this.GetValIntByKey(DictFlowAttr.Idx);
    }
    public final void setIdx(int value) throws Exception
    {
        SetValByKey(DictFlowAttr.Idx, value);
    }
    public DictFlow(){}
    public Map getEnMap() throws Exception {
        if (this.get_enMap() != null) {
            return this.get_enMap();
        }
        Map map = new Map("Frm_DictFlow", "台账子流程");

        map.AddMyPK();

        map.AddTBString(DictFlowAttr.FrmID, null, "表单ID", true, false, 0, 300, 10);
        map.AddTBString(DictFlowAttr.FlowNo, null, "流程编号", true, false, 0, 20, 10);
        map.AddTBString(DictFlowAttr.Label, null, "功能标签", true, false, 0, 20, 10);
        map.AddTBInt(DictFlowAttr.IsShowListRight, 0, "是否显示在列表右边", true, false);

        map.AddTBInt(DictFlowAttr.Idx, 0, "Idx", true, false);
        this.set_enMap(map);
        return this.get_enMap();
    }
    public void DoUp() throws Exception{
        this.DoOrderUp(DictFlowAttr.FrmID, this.getFrmID(), DictFlowAttr.Idx);
    }
    public void DoDown() throws Exception
    {
        this.DoOrderDown(DictFlowAttr.FrmID, this.getFrmID(), DictFlowAttr.Idx);
    }
}
