package bp.gpm;

import bp.da.DBAccess;
import bp.da.Depositary;
import bp.en.EnType;
import bp.en.EntityMyPK;
import bp.en.Map;
import bp.en.UAC;

public class PowerCenter extends EntityMyPK {
    /// <summary>
    /// 控制主键
    /// </summary>
    public final String  getCtrlPKVal() throws Exception
    {
        return this.GetValStringByKey(PowerCenterAttr.CtrlPKVal);
    }
    public final String getIDNames() throws Exception
    {
        return this.GetValStrByKey(PowerCenterAttr.IDNames);
    }
    public final String getIDs() throws Exception
    {
        return this.GetValStrByKey(PowerCenterAttr.IDs);
    }
    public final String getCtrlModel() throws Exception
    {
        return this.GetValStrByKey(PowerCenterAttr.CtrlModel);
    }
    public final String CtrlGroup() throws Exception
    {
        return this.GetValStrByKey(PowerCenterAttr.CtrlGroup);
    }
    public final String getCtrlObj() throws Exception
    {
        return this.GetValStrByKey(PowerCenterAttr.CtrlObj);
    }

    public UAC getHisUAC() throws Exception
    {
        UAC uac = new UAC();
        uac.OpenForAppAdmin();
        return uac;
    }

    /// <summary>
    /// 权限中心
    /// </summary>
    public PowerCenter()
    {
    }
    /// <summary>
    /// 权限中心
    /// </summary>
    /// <param name="mypk"></param>
    public PowerCenter(String no) throws Exception
    {
        this.setMyPK(no);
        this.Retrieve();
    }
    /// <summary>
    /// EnMap
    /// </summary>
    @Override
    public Map getEnMap() throws Exception
    {
        if (this.get_enMap() != null)
        {
            return this.get_enMap();
        }

        Map map = new Map("GPM_PowerCenter");
        map.setDepositaryOfEntity( Depositary.None);
        map.setDepositaryOfMap(Depositary.Application);
        map.setEnDesc("权限中心");
        map.setEnType(EnType.Sys);

        map.AddMyPK();

        // System,Module,Menus
        map.AddTBString(PowerCenterAttr.CtrlObj, null, "控制对象(SystemMenus)", true, false, 0, 300, 20);
        map.AddTBString(PowerCenterAttr.CtrlPKVal, null, "控制对象ID", true, false, 0, 300, 20);
        //Menus, Frm
        map.AddTBString(PowerCenterAttr.CtrlGroup, null, "隶属分组(可为空)", true, false, 0, 300, 20);

        //AnyOne,Adminer,Depts
        map.AddTBString(PowerCenterAttr.CtrlModel, null, "控制模式", true, false, 0, 300, 20);

        map.AddTBStringDoc(PowerCenterAttr.IDs, null, "主键s(Stas,Depts等)", true, false);
        map.AddTBStringDoc(PowerCenterAttr.IDNames, null, "IDNames", true, false);

        map.AddTBString(PowerCenterAttr.OrgNo, null, "编号", true, false, 0, 100, 20);
        map.AddTBString(PowerCenterAttr.Idx, null, "Idx", true, false, 0, 100, 20);

        this.set_enMap(map);
        return this.get_enMap();
    }

    @Override
    protected  boolean beforeUpdateInsertAction() throws Exception
    {
        if (bp.web.WebUser.getIsAdmin() == false)
            throw new Exception("err@非管理员不能操作...");

        return super.beforeUpdateInsertAction();
    }
    @Override
    protected  boolean beforeInsert() throws Exception
    {
        this.setMyPK(DBAccess.GenerGUID());
        return super.beforeInsert();
    }
}
