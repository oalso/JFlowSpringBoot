package bp.gpm;

import bp.da.*;
import bp.web.*;
import bp.en.*;
import java.util.*;

/** 
 人员菜单功能
*/
public class VGPMEmpMenuAttr
{
	/** 
	 操作员
	*/
	public static final String FK_Emp = "FK_Emp";
	/** 
	 菜单功能
	*/
	public static final String FK_Menu = "FK_Menu";
	/** 
	 系统
	*/
	public static final String FK_App = "FK_App";
}