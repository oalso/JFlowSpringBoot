package bp.gpm.home;

import bp.en.EntityNoNameAttr;

/** 
信息块
*/
public class WindowTemplateAttr extends EntityNoNameAttr
{
	/** 
	 信息块类型
	*/
	public static final String Icon = "Icon";
	/** 
	 顺序
	*/
	public static final String Idx = "Idx";
	/** 
	 标题
	*/
	public static final String Docs = "Docs";
	public static final String WinDocType = "WinDocType";
	/** 
	 tag1
	*/
	public static final String ColSpan = "ColSpan";
	/** 
	 Tag2
	*/
	public static final String MoreLinkModel = "MoreLinkModel";
	/** 
	 PopW
	*/
	public static final String PopW = "PopW";
	public static final String PopH = "PopH";

	/** 
	 是否可删除
	*/
	public static final String IsDel = "IsDel";
	/** 
	 控制方式
	*/
	public static final String CtrlWay = "CtrlWay";
	/** 
	 打开方式
	*/
	public static final String OpenWay = "OpenWay";
	/** 
	 更多标签
	*/
	public static final String MoreLab = "MoreLab";
	/** 
	 MoreUrl
	*/
	public static final String MoreUrl = "MoreUrl";
	/** 
	 产生时间
	*/
	public static final String DocGenerRDT = "DocGenerRDT";
	/** 
	 是否独占一行
	*/
	public static final String IsEnable = "IsEnable";
	/** 
	 组织编号
	*/
	public static final String OrgNo = "OrgNo";
	/** 
	 权限控制方式.
	*/
	public static final String WindCtrlWay = "WindCtrlWay";
	 /// <summary>
    /// 页面ID.
    /// </summary>
    public static final String PageID = "PageID";
  /// <summary>
    /// 模式
    /// </summary>
    public static final String WinDocModel = "WinDocModel";
  //C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
  ///#region 数据源
  		public static final String DBType = "DBType";
  		public static final String DBSrc = "DBSrc";
  		public static final String DBExp0 = "DBExp0";
  		public static final String DBExp1 = "DBExp1";
  		public static final String DBExp2 = "DBExp2";
  //C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
  		///#endregion 数据源

  //C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
  		///#region 百分比扇形图
  		public static final String LabOfFZ = "LabOfFZ";
  		public static final String SQLOfFZ = "SQLOfFZ";
  		public static final String LabOfFM = "LabOfFM";
  		public static final String SQLOfFM = "SQLOfFM";
  		public static final String LabOfRate = "LabOfRate";
  //C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
  		///#endregion 百分比扇形图
}
