package bp.gpm.home.windowext;

import bp.en.EntityNoNameAttr;

public class DtlAttr extends EntityNoNameAttr
{
		public static final String FontColor = "FontColor";
		public static final String DBSrc = "DBSrc";
		public static final String DBType = "DBType";
		public static final String Exp0 = "Exp0";
		public static final String Exp1 = "Exp1";

		public static final String RefWindowTemplate = "RefWindowTemplate";
		 /// <summary>
        /// 显示类型
        /// </summary>
        public static final String WindowsShowType = "WindowsShowType";

}
