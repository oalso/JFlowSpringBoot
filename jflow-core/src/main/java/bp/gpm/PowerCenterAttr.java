package bp.gpm;

import bp.en.EntityNoNameAttr;

public class PowerCenterAttr  extends EntityNoNameAttr {
    /// <summary>
    /// 控制对象
    /// </summary>
    public static final String CtrlObj = "CtrlObj";
    /// <summary>
    /// 分组
    /// </summary>
    public static final String CtrlGroup = "CtrlGroup";
    /// <summary>
    /// 控制模式
    /// </summary>
    public static final String CtrlModel = "CtrlModel";
    /// <summary>
    /// IDs
    /// </summary>
    public static final String IDs = "IDs";
    /// <summary>
    /// 名称
    /// </summary>
    public static final String IDNames = "IDNames";
    /// <summary>
    /// 控制对象Val
    /// </summary>
    public static final String CtrlPKVal = "CtrlPKVal";
    /// <summary>
    /// 编号
    /// </summary>
    public static final String OrgNo = "OrgNo";

    public static final String Idx = "Idx";
}
