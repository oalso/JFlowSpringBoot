package bp.gpm;

/** 
部门菜单
*/
public class DeptMenuAttr
{
	/** 
	 菜单
	*/
	public static final String FK_Menu = "FK_Menu";
	/** 
	 部门
	*/
	public static final String FK_Dept = "FK_Dept";
	/** 
	 是否选中.
	*/
	public static final String IsChecked = "IsChecked";
}