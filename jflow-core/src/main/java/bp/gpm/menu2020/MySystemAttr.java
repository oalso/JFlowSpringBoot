package bp.gpm.menu2020;

import bp.en.EntityNoNameAttr;

/** 
系统
*/
public class MySystemAttr extends EntityNoNameAttr
{
	/** 
	 顺序
	*/
	public static final String Idx = "Idx";
	/** 
	 应用类型
	*/
	public static final String MySystemModel = "MySystemModel";
	/** 
	 UrlExt
	*/
	public static final String UrlExt = "UrlExt";
	/** 
	 SubUrl
	*/
	public static final String OrgNo = "OrgNo";
	/** 
	 是否启用.
	*/
	public static final String IsEnable = "IsEnable";
	/** 
	 FK_MySystemSort
	*/
	public static final String FK_MySystemSort = "FK_MySystemSort";
	/** 
	 关联菜单编号
	*/
	public static final String RefMenuNo = "RefMenuNo";
	/** 
	 用户控件ID
	*/
	public static final String UidControl = "UidControl";
	/** 
	 密码控件ID
	*/
	public static final String PwdControl = "PwdControl";
	/** 
	 提交方式
	*/
	public static final String ActionType = "ActionType";
	/** 
	 登录方式
	*/
	public static final String SSOType = "SSOType";
	public static final String Icon = "Icon";
}


