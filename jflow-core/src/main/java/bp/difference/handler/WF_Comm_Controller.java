package bp.difference.handler;

import javax.servlet.http.HttpServletRequest;

import bp.da.DataType;
import bp.da.LogType;
import bp.web.WebUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;

import bp.wf.httphandler.WF_Comm;

@Controller
@RequestMapping("/WF/Comm")
@ResponseBody
public class WF_Comm_Controller extends HttpHandlerBase {

	/**
	 * 默认执行的方法
	 * 
	 * @return
	 */
	@RequestMapping(value = "/ProcessRequest")
	public final void ProcessRequest(HttpServletRequest request) {
		try {
			if (DataType.IsNullOrEmpty(WebUser.getNo())){
				String sid=this.GetRequestVal("SID");
				if(DataType.IsNullOrEmpty(sid)==false)
					bp.wf.Dev2Interface.Port_LoginBySID(sid);
				String userNo=this.GetRequestVal("userNo");
				if(DataType.IsNullOrEmpty(userNo)==false)
					bp.wf.Dev2Interface.Port_Login(userNo);
			}
			else{
				String userNo=this.GetRequestVal("userNo");
				bp.da.Log.DefaultLogWriteLine(LogType.Info,"commNo1："+userNo);
				if(!DataType.IsNullOrEmpty(userNo)&&!userNo.equals("undefined")){
					bp.port.Emp emp=new bp.port.Emp(userNo);
					if(emp.RetrieveFromDBSources()>=0)
						bp.wf.Dev2Interface.Port_Login(userNo);
				}
				bp.da.Log.DefaultLogWriteLine(LogType.Info,"commNo2："+WebUser.getNo());
			}
		}
		catch (Exception ex){

		}
		WF_Comm CommHandler = new WF_Comm();
		if (request instanceof DefaultMultipartHttpServletRequest) {
			//如果是附件上传Request，则将该Request放入全局Request。为了解决springmvc中全局Request无法转化为附件Request
			HttpServletRequest request1 = CommonUtils.getRequest();
			request1.setAttribute("multipartRequest", request);
		}
		super.ProcessRequestPost(CommHandler);
	}

	@Override
	public Class<WF_Comm> getCtrlType() {
		return WF_Comm.class;
	}

}
